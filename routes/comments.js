const express = require('express');
const config = require('../config/config');
const db = require('../config/database');
const router = express.Router();

// Get Comments to a single Post:
router.get(config.api_base_url + '/posts/:id/comments', function(req, res)
{
    db.all("SELECT * FROM Comment WHERE post_id=?", req.params.id, function(err, rows){
        res
            .status(200)
            .type('application/json')
            .send(config.result_object(200, rows, []));
    });
});

// Get single Comment to a single Post:
router.get(config.api_base_url + '/posts/:id/comments/:commentid', function(req, res)
{
    db.all("SELECT * FROM Comment WHERE post_id=? AND id=?", req.params.id, req.params.commentid, function(err,rows){
        res
            .status(200)
            .type('application/json')
            .send(config.result_object(200, rows, [err]));
    });
});

// Create a Comment to a single Post:
router.post(config.api_base_url + '/posts/:id/comments', function(req, res)
{
    db.serialize(function(){
        let stmt = db.prepare("INSERT INTO Comment(message, post_id) VALUES(?,?)");
        stmt.run(req.body.message, req.params.id);
        stmt.finalize(function(err){
            res
                .status(204)
                .send([]);
        });
    });
});

router.delete(config.api_base_url + '/posts/:id/comments/:commentid', function(req, res) {
    console.log("DELETE COMMENT");
    db.run("DELETE FROM Comment WHERE post_id=? AND id=?", req.params.id, req.params.commentid, function(err) {
        res
            .status(204)
            .send();
    });
});

module.exports = router;