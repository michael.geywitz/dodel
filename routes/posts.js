const express = require('express');
const config = require('../config/config');
const db = require('../config/database');
const router = express.Router();

// Get all Posts:
router.get(config.api_base_url + '/posts', function(req, res)
{
    db.all("SELECT *,(SELECT COUNT(*) FROM Comment c WHERE c.post_id=p.id) as no_of_comments FROM Post p ORDER BY votes DESC", function(err, rows){
        res
            .status(200)
            .type('application/json')
            .send(config.result_object(200, rows, []));
    });
});

// Get single Post:
router.get(config.api_base_url + '/posts/:id', function(req, res)
{
    db.all("SELECT * FROM Post WHERE id=?", req.params.id, function(err, rows){
        res
            .status(200)
            .type('application/json')
            .send(config.result_object(200, rows, err));
    });
});

// Create new Post:
router.post(config.api_base_url + '/posts', function(req, res)
{
    db.serialize(function(){
        db.run("INSERT INTO Post(message,votes) VALUES(?,?)", req.body.message, 0, function(err){
            db.get("SELECT * FROM Post WHERE id=?", this.lastID, function(err, row) {
                res
                    .status(201)
                    .type('application/json')
                    .send(config.result_object(201, row, []));
            });
        });
    });
});

// Update existing single Post:
router.put(config.api_base_url + '/posts/:id', function(req, res)
{
    db.serialize(function() {
        let stmt = "";
        let parameters = [];
        for(var key in req.body)
        {
            // Ignore ID-Field, Handle Votes -> up and down:
            if(key != "id") {
                if(key == "votes") {
                    stmt += key + "=" + key + "+?,";
                }
                else {
                    stmt += key + "=?,";
                }
                parameters.push(req.body[key]);
            }
        }
        stmt = "UPDATE Post SET " + stmt.replace(/,\s*$/, "") + " WHERE id=?";
        parameters.push(req.params.id);

        db.run(stmt, parameters, function(err) {
            db.get("SELECT * FROM Post WHERE id=?", req.params.id, function(err, row) {
                res
                    .status(201)
                    .type('application/json')
                    .send(config.result_object(201, row, []));
            });
        });
    });
});

// Delete single Post:
router.delete(config.api_base_url + '/posts/:id', function(req, res)
{
    db.serialize(function(){
        let stmt = db.prepare("DELETE FROM Post WHERE id=?");
        let errors = []
        stmt
            .run(req.params.id, function(err){
                errors.push(err);
            })
            .finalize(function(err){
                res
                    .status(204)
                    .type('application/json')
                    .send();
            });
    });
});

module.exports = router;