const config = {
    application_port: 80,
    api_base_url: '/api/v1',
    result_object: function(status, data, errors) {
        return {
            status: status,
            length: data.length,
            result: data,
            errors: []
        }
    },
    database_path: './data/dodel.db'
}

module.exports = config;