// Include Packages:
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
const config = require('../config/config');

// Create Database Object:
let db = new sqlite3.Database(config.database_path, function(err){
    if(err)
    {
        console.log(err);
    }
    console.log("Connected to Database.");
});

db.serialize(function(){
    if(!fs.existsSync(config.database_path))
    {
        db.run("CREATE TABLE Post(id INTEGER PRIMARY KEY AUTOINCREMENT, message VARCHAR(255), votes INTEGER)", function(err) {
            if(err) {
                console.log("ERROR: " + err);
            }
            console.log("Created Post-Table.");
        });
        
        db.run("CREATE TABLE Comment(id INTEGER PRIMARY KEY AUTOINCREMENT, message VARCHAR(255), post_id INTEGER, FOREIGN KEY(post_id) REFERENCES Post(id))", function(err) {
            if(err) {
                console.log("ERROR: " + err);
            }
            console.log("Created Comment-Table.");
        });     

        db.run("CREATE TABLE User(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(255) UNIQUE, password VARCHAR(255))");
    }
});

module.exports = db;