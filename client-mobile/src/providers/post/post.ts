import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PostProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostProvider {

  private url : string = "http://localhost/api/v1/posts";

  constructor(public http: HttpClient) {
  }

  public getPosts() {
    return this.http.get(this.url);
  }

  public getPost(id) {
    return this.http.get(this.url + '/' + id);
  }

  public postPost(data) {
    return this.http.post(this.url, data);
  }

  public postUpvote(id) {
    return this.http.put(this.url + '/' + id, {votes : 1});
  }

  public postDownvote(id) {
    return this.http.put(this.url + '/' + id, {votes: -1});
  }

  public deletePost(id) {
    return this.http.delete(this.url + '/' + id);
  }

}
