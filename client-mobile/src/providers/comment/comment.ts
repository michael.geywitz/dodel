import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CommentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommentProvider {

  private baseUrl : string = "http://localhost/api/v1/posts/";

  constructor(public http: HttpClient) {
  }

  public getComments(postId) {
    return this.http.get(this.baseUrl + postId + "/comments");
  }

  public postComment(postId, data) {
    return this.http.post(this.baseUrl + postId + "/comments", data);
  }

  public deleteComment(postId, commentId) {
    return this.http.delete(this.baseUrl + postId + "/comments/" + commentId);
  }

}
