import { Component } from '@angular/core';
import { NavController, LoadingController, ActionSheetController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { CommentsPage } from '../comments/comments';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PostProvider]
})
export class HomePage {

  posts : any = [];
  postMessageNew : String = '';

  constructor(
    public navCtrl: NavController, 
    public post : PostProvider, 
    public loadingCtrl : LoadingController,
    public actionSheetCtrl : ActionSheetController
  ) {
    this.loadPosts();
  }

  createAction() {
    this.post.postPost({message: this.postMessageNew}).subscribe(data => {
      this.postMessageNew = '';
      this.loadPosts();
    });
  }

  upvoteAction(id) {
    this.post.postUpvote(id).subscribe(data => {
      this.loadPosts();
    });
  }

  downvoteAction(id) {
    this.post.postDownvote(id).subscribe(data => {
      this.loadPosts();
    });
  }

  deleteAction(id) {
    this.post.deletePost(id).subscribe(data => {
      this.loadPosts();
    });
  }

  loadPosts() {
    this.post.getPosts().subscribe((data : any) => {
      this.posts = data.result;
      const loader = this.loadingCtrl.create({
        content: "Bitte warten ...",
        duration: 500
      });
      loader.present();
    });
  }

  showMenu(id) {
    let menu = this.actionSheetCtrl.create({
      title: "Post#" + id,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          icon: 'chatboxes',
          handler: () => {
            this.navCtrl.push(CommentsPage, {
              'post_id': id
            });
          }
        },
        {
          icon: 'thumbs-up',
          handler: () => {
            this.upvoteAction(id);
          }
        },
        {
          icon: 'thumbs-down',
          handler: () => {
            this.downvoteAction(id);
          }
        },
        {
          icon: 'trash',
          handler: () => {
            this.deleteAction(id);
          }
        }
      ]
    }).present();
  }


}
