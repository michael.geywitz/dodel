import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { CommentProvider } from '../../providers/comment/comment';

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  currentPost : any = {};
  commentMessageNew : string = "";
  comments : any = [];
  title : string = "";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public post : PostProvider,
    public comment : CommentProvider,
    public actionSheetCtrl : ActionSheetController
  ) {
  }

  ionViewDidLoad() {
    this.title = "Comments of Post#" + this.navParams.get('post_id');
    
    this.post.getPost(this.navParams.get('post_id')).subscribe((data : any) => {
      this.currentPost = data.result[0];
    });

    this.loadComments();
  }

  createAction() {
    this.comment.postComment(this.navParams.get('post_id'), {message: this.commentMessageNew}).subscribe((data) => {
      this.loadComments();
      this.commentMessageNew = "";
    });
  }

  loadComments() {
    this.comment.getComments(this.navParams.get('post_id')).subscribe((comments : any) => {
      this.comments = comments.result;
    });
  }

  showMenu(id) {
    let menu = this.actionSheetCtrl.create({
      title: "Comment#" + id,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          icon: 'trash',
          handler: () => {
            this.comment.deleteComment(this.navParams.get('post_id'), id).subscribe(() => {
              this.loadComments();
            });
          }
        }
      ]
    }).present();
  }

}
