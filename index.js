// Webprogrammierung Jodel API - index.js
// Author: Michael Geywitz
// Date: 02.11.18

// Include Packages:
const bodyParser = require('body-parser');
const express = require('express');
const cookieParser = require('cookie-parser');
const uuid = require('uuid');

// Include Configurations:
const config = require('./config/config');
const db = require('./config/database');

// Include Routes:
const postRoutes = require('./routes/posts');
const commentRoutes = require('./routes/comments');
const loginRoutes = require('./routes/login');

// Initialize App:
const app = express();
const sessions = [];

// Register Middlewares:
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use((req,res,next) => {
    if(req.cookies.token != undefined && sessions.indexOf(req.cookies.token) !== -1) {
        req.isAuthenticated = true;
    }
    else {
        req.isAuthenticated = false;
    }
    next();
});

app.use(postRoutes);
app.use(commentRoutes);
app.use(loginRoutes);

// Start to listen on the defined Port:
app.listen(config.application_port, function() {
    console.log("Jodel-Server started to listen on Port " + config.application_port + ".");
});